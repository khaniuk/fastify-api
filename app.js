"use strict";

require("dotenv").config();

const fastify = require("fastify")({ logger: true });
const port = process.env.PORT || 3000;

fastify.register(require("fastify-cors"), {
  origin: "*",
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
});

/* fastify.register(require("point-of-view"), {
  engine: {
    handlebars: require("handlebars"),
  },
});

fastify.register(require("fastify-static"), {
  root: path2.join(__dirname, "public"),
  prefix: "public/",
});

fastify.get("/", (request, reply) => {
  const name = request.query.name || "Anonymous";
  reply.view("./templates/index.hbs", { name });
}); */

fastify.register(require("./src/routes/index"), {
  prefix: "/api",
});

const start = async () => {
  try {
    await fastify.listen(port);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
