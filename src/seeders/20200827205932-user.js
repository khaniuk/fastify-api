"use strict";

const { hashSync, genSaltSync } = require("bcrypt");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "users",
      [
        {
          roleId: 1,
          name: "MiYo",
          correo: "admin@test.com",
          password: hashSync("admin", genSaltSync(10), null),
          status: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("users", null, {});
  },
};
