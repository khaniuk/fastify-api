const { user, role } = require("../models");
const BaseRepository = require("./baseRepository");

exports.getAll = async () => {
  const query = {
    attributes: ["id", "name", "email", "status"],
    order: [["id", "ASC"]],
  };
  const result = await BaseRepository.getAll(user, query);
  return result;
};

exports.getOne = async (data) => {
  const query = {
    email: data.email,
  };
  const result = await BaseRepository.getOne(user, query);
  return result;
};

exports.create = async (data) => {
  const query = {
    name: data.name,
    email: data.email,
    status: data.status,
  };
  const result = await BaseRepository.create(user, query);
  return result;
};

exports.update = async (id, data) => {
  let result = false;
  const item = await BaseRepository.getById(user, id);
  if (item) {
    let newData = {
      rol_id: data.rol_id || usuario.rol_id,
      name: data.name || usuario.name,
      email: data.email || usuario.email,
      status: data.status,
    };
    if (data.password && data.password != "") {
      newData.clave = data.clave;
    }
    result = await BaseRepository.update(item, newData);
  }
  return result;
};

exports.delete = async (id) => {
  let result = false;
  const item = await BaseRepository.getById(user, id);
  if (item) {
    result = await BaseRepository.delete(item);
  }
  return result;
};

exports.auth = async (data) => {
  let result = false,
    token = "";
  const query = {
    where: { email: data.email },
    attributes: ["id", "name"],
    include: {
      model: role,
      required: true,
      where: { estado: true },
      attributes: ["id", "description"],
    },
  };

  const resultUser = await BaseRepository.getOne(user, query);
  if (resultUser) {
    const passwordCheck = await resultUser.comparePassword(data.password);
    if (passwordCheck) {
      result = {
        ius: resultUser.id,
        iro: resultUser.roleId,
      };
      token = await sign({ data: JSON.stringify(response) }, apikey, {
        //token = await sign(datos, apikey, {
        expiresIn: 86400 * 30,
      });
      result.status = "success";
      result.name = user.name;
      result.role = resultUser.role ? resultUser.role.description : "";
      result.token = token;
      result.message = "Auth success";
      return result;
    } else {
      result = {
        status: "error",
        token: token,
        message: "incorrect access data",
      };
    }
  }
  return result;
};
