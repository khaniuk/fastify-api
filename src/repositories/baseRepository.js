"use strict";

module.exports = {
  async getAll(entity, query) {
    const result = await entity.findAll(query);
    return result;
  },

  getOne(entity, query) {
    return entity.findOne(query);
  },

  getById(entity, id) {
    return entity.findByPk(id);
  },

  create(entity, data) {
    return entity.create(data);
  },

  update(entity, data) {
    return entity.update(data);
  },

  delete(entity) {
    return entity.destroy();
  },
};
