const { role } = require("../models");
const BaseRepository = require("./baseRepository");

exports.getAll = async () => {
  const query = {
    attributes: ["id", "description", "status"],
    order: [["id", "ASC"]],
  };
  const result = await BaseRepository.getAll(role, query);
  return result;
};

exports.create = async (data) => {
  const query = {
    description: data.description,
    status: data.status,
  };
  const result = await BaseRepository.create(role, query);
  return result;
};

exports.update = async (id, data) => {
  let result = false;
  const item = await BaseRepository.getById(role, id);
  if (item) {
    const newData = {
      description: data.description || item.description,
      status: data.status,
    };
    result = await BaseRepository.update(item, newData);
  }
  return result;
};

exports.delete = async (id) => {
  let result = false;
  const item = await BaseRepository.getById(role, id);
  if (item) {
    result = await BaseRepository.delete(item);
  }
  return result;
};
