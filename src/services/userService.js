const UserRepository = require("../repositories/userRepository");

exports.getAll = () => {
  return UserRepository.getAll();
};

exports.getOne = (data) => {
  return UserRepository.getOne(data);
};

exports.create = (data) => {
  return UserRepository.create(data);
};

exports.update = (id, data) => {
  return UserRepository.update(id, data);
};

exports.delete = (id) => {
  return UserRepository.delete(id);
};
