const RoleRepository = require("../repositories/roleRepository");

exports.getAll = () => {
  return RoleRepository.getAll();
  /* return new Promise((resolve, reject) => {
    const result = RoleRepository.getAll(query);
    if (result) {
      resolve(result);
    } else {
      reject(result);
    }
  }); */
};

exports.create = (data) => {
  return RoleRepository.create(data);
};

exports.update = (id, data) => {
  return RoleRepository.update(id, data);
};

exports.delete = (id) => {
  return RoleRepository.delete(id);
};
