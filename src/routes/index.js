// const AuthMiddleware = require("../middlewares/authMiddleware");
// const multerDocumento = require("../middlewares/uploadDocument");
const RoleController = require("../controllers/roleController");
const UserController = require("../controllers/userController");
const AuthController = require("../controllers/authController");

module.exports = async (fastify, options) => {
  fastify.get("/", function (req, replay) {
    replay.render("index", {
      title: "BACKEND API",
    });
  });

  fastify.get(
    "/roles",
    // AuthMiddleware.checkToken,
    RoleController.getRoles
  );

  fastify.post(
    "/role",
    //   AuthMiddleware.checkToken,
    RoleController.createRole
  );
  fastify.put(
    "/role/:id",
    //   AuthMiddleware.checkToken,
    RoleController.updateRole
  );
  fastify.delete(
    "/role/:id",
    //   AuthMiddleware.checkToken,
    RoleController.deleteRole
  );

  fastify.get(
    "/users",
    // AuthMiddleware.checkToken,
    UserController.getUsers
  );
  fastify.post(
    "/user",
    //   AuthMiddleware.checkToken,
    UserController.createUser
  );
  fastify.put(
    "/user/:id",
    //   AuthMiddleware.checkToken,
    UserController.updateUser
  );
  fastify.delete(
    "/user/:id",
    //   AuthMiddleware.checkToken,
    UserController.deleteUser
  );
  fastify.post("/user/auth", AuthController.userAuth);

  //   done();
};
