const UserService = require("../services/userService");
const {
  statusCode,
  responseText,
  sendData,
  sendMessage,
  sendError,
} = require("../utils/response");

exports.userAuth = (req, res) => {
  UserService.auth(req.body)
    .then((response) => {
      if (response) {
        return sendMessage(res, statusCode.notFound, responseText.notFound);
      }
      sendData(res, statusCode.success, response);
    })
    .catch((err) => sendError(res, statusCode.internalError, err));
};
