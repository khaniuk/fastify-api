const UserService = require("../services/userService");
const {
  statusCode,
  responseText,
  sendData,
  sendMessage,
  sendError,
} = require("../utils/response");

exports.getUsers = async (req, res) => {
  UserService.getAll()
    .then((response) => sendData(res, statusCode.success, response))
    .catch((err) => sendError(res, statusCode.internalError, err));
};

exports.getUser = async (req, res) => {
  UserService.getOne(req.body)
    .then((response) => sendData(res, statusCode.success, response))
    .catch((err) => sendError(res, statusCode.internalError, err));
};

exports.createUser = (req, res) => {
  UserService.create(req.body)
    .then(() => sendMessage(res, statusCode.created, responseText.created))
    .catch((err) => sendError(res, statusCode.internalError, err));
};

exports.updateUser = (req, res) => {
  UserService.update(req.params.id, req.body)
    .then((response) => {
      if (!response) {
        return sendMessage(res, statusCode.notFound, responseText.notFound);
      }
      sendMessage(res, statusCode.success, responseText.updated);
    })
    .catch((err) => sendError(res, statusCode.internalError, err));
};

exports.deleteUser = (req, res) => {
  UserService.delete(req.params.id)
    .then((response) => {
      if (!response) {
        return sendMessage(res, statusCode.notFound, responseText.notFound);
      }
      sendMessage(res, statusCode.deleted, responseText.deleted);
    })
    .catch((err) => sendError(res, statusCode.internalError, err));
};
