const RoleService = require("../services/roleService");
const {
  statusCode,
  responseText,
  sendData,
  sendMessage,
  sendError,
} = require("../utils/response");

exports.getRoles = async (req, res) => {
  RoleService.getAll()
    .then((response) => sendData(res, statusCode.success, response))
    .catch((err) => sendError(res, statusCode.internalError, err));
};

exports.createRole = (req, res) => {
  RoleService.create(req.body)
    .then(() => sendMessage(res, statusCode.created, responseText.created))
    .catch((err) => sendError(res, statusCode.internalError, err));
};

exports.updateRole = (req, res) => {
  RoleService.update(req.params.id, req.body)
    .then((response) => {
      if (!response) {
        return sendMessage(res, statusCode.notFound, responseText.notFound);
      }
      sendMessage(res, statusCode.success, responseText.updated);
    })
    .catch((err) => sendError(res, statusCode.internalError, err));
};

exports.deleteRole = (req, res) => {
  RoleService.delete(req.params.id)
    .then((response) => {
      if (!response) {
        return sendMessage(res, statusCode.notFound, responseText.notFound);
      }
      sendMessage(res, statusCode.deleted, responseText.deleted);
    })
    .catch((err) => sendError(res, statusCode.internalError, err));
};
