"use strict";

const { hashSync, genSaltSync, compare } = require("bcrypt");
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    static associate(models) {
      user.belongsTo(models.role, {
        foreignKey: "roleId",
      });
    }
  }
  user.init(
    {
      roleId: DataTypes.INTEGER,
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      status: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      underscored: true,
      modelName: "user",
      schema: "public",
    }
  );
  user.beforeCreate(async (user, options) => {
    const hashedPassword = await hashSync(user.password, genSaltSync(10), null);
    user.password = hashedPassword;
  });
  user.beforeUpdate(async (user, options) => {
    const hashedPassword = await hashSync(user.password, genSaltSync(10), null);
    user.password = hashedPassword;
  });
  user.prototype.comparePassword = async function (password) {
    return await compare(password, this.password);
  };

  return user;
};
