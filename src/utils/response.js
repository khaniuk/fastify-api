"use strict";

exports.statusCode = {
  success: 200,
  created: 201,
  accepted: 202,
  // notAuthoritative: 203,
  deleted: 204,
  badRequest: 400,
  unauthorizated: 401,
  forbidden: 403,
  notFound: 404,
  internalError: 500,
  badGateway: 502,
};

exports.responseText = {
  created: "Record created",
  updated: "Record updated",
  deleted: "Record deleted",
  notFound: "Record not found",
  exist: "Record exist",
};

exports.sendData = (res, code, data) => {
  return res.status(code).send(data);
};

exports.sendMessage = (res, code, message = null) => {
  return res.status(code).send(message);
};

exports.sendError = (res, code, error) => {
  return res.status(code).send(error.message);
};
